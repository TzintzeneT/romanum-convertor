#!/bin/bash

cp RomanNumbers.sh romanum
chmod +x romanum
sudo mv romanum /bin/
if [[ $? -eq 0  ]]; then
  echo "installed successfully! you can now use the command 'romanum' to calculate roman numbers"
else
  echo "couldn't install romanum :("
fi
