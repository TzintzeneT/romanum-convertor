#!/bin/bash

fromRoman(){

  #echo "fromRoman activated"

  #create a map with a value for each Roman digit
  declare -A values

  values["I"]=1
  values["V"]=5
  values["X"]=10
  values["L"]=50
  values["C"]=100
  values["D"]=500
  values["M"]=1000

  #variable for calculating the value of the number
  sum=0

  #make sure all letters are uppercase
  num="${num^^}"

  #go over and calculate the value of each digit
  for ((i=0; i<${#num}; i++)); do

    #check that the letter is valid
    while [[ ${#num}>0 && -z ${values[${num:i:1}]} ]]; do
      echo -e "\e[1;31mError at: letter ${num:i:1}:\e[0;31m this letter is not valid\e[;m"
      num="${num//${num:i:1}/}"
    done
    #check that the next letter is valid so that compering to it will work as intended
    while [[ $i+1 -lt ${#num} && -z ${values[${num:i+1:1}]} ]]; do
      echo -e "\e[1;31mError at: letter ${num:i+1:1}:\e[0;31m this letter is not valid\e[;m"
      num="${num//${num:i+1:1}/}"
    done

    #calculate the value of the digit
    if [[ ${#num}>0 && $i+1 -lt ${#num} && ${values[${num:i:1}]} -lt ${values[${num:i+1:1}]} ]]; then
      #Subtract the value from sum
      let sum-=${values[${num:i:1}]} #|| LETTER_IS_NOT_VALID
      #check to see if the user requested to see the calculations
      if [[ $option2 == "-v" || $option2 == "--val" || $option2 == "--values" ]]; then
        #print the values
        #printf "\e[1;32m${num:i:1}:\e[;m -"
        echo -e "\e[1;32m${num:i:1}:\e[;m -${values[${num:i:1}]}"
      fi
    elif [[ ${#num}>0 ]]; then
      #add the value to sum
      let sum+=${values[${num:i:1}]} #|| LETTER_IS_NOT_VALID
      #check to see if the user requested to see the calculations
      if [[ $option2 == "-v" || $option2 == "--val" || $option2 == "--values" ]]; then
        #print the values
        #printf "\e[1;32m${num:i:1}:\e[;m -"
        echo -e "\e[1;32m${num:i:1}:\e[;m ${values[${num:i:1}]}"
      fi
    fi
  done

  printf "$num"
  echo " is $sum"
}

#convert a number to Roman numbers
toRoman(){

  #echo "toRoman activated"

  #all of the Roman digits
  romanNums="IVXLCDM"
  #variable to store the result
  result=""
  #make sure the number is not seen as an octal number
  num=${num#0}

  #check if the number is above 0
  if [[ $num -lt 0 ]]; then
    echo -e "\e[1;33mWarning:\e[0;33m Roman numbers cannot be negative\e[;m"
    let num*=-1
  fi

  #check that the number is not too big
  if [[ $num -gt 3999 ]]; then
    echo -e "\e[1;33mWarning:\e[0;33m Roman numbers cannot be more than 3999, only the last 3 or 4 digits will be calculated.\e[;m"
    if [[ num%10000 -gt 3999 ]]; then
      let num%=1000
    else
      let num%=10000
    fi
  fi

  printf "$num"
  #go over and calculate the value of each digit
  for ((i=0 ;$num > 0; num=num/10, i+=2 )); do

    if [[ 4 -gt num%10 ]]; then
      #echo "less then 4"
      for (( j = num%10; j > 0; j-- )); do
        result="${romanNums:i:1}$result"
      done

    elif [[ 4 -eq num%10 ]]; then
      #echo "is 4"
      result="${romanNums:i+1:1}$result"
      result="${romanNums:i:1}$result"

    elif [[ 9 -gt num%10 ]]; then
      #echo "less then 9"
      for (( j = num%10-5; j > 0; j-- )); do
        result="${romanNums:i:1}$result"
      done
      result="${romanNums:i+1:1}$result"

    else
      #echo "is 9"
      result="${romanNums:i+2:1}$result"
      result="${romanNums:i:1}$result"
    fi
  done
  #print the result
  echo " is $result"
}

#variables for the options
num=$1
option2=$2

#the --help option
if [[ $1 == "-h" || $1 == "--help" || -z $1 ]]; then
  echo
  echo -e "\e[1;36mHelp center\e[;m"
  echo
  echo -e "\e[1;32m Commands:\e[;m"
  echo "    [-h][--help]: Help center"
  echo "    [-a][--about]: a page about this program"
  echo "    [-v][--val][--values]: this command show the values of the Roman numbers and can be used alone or as a second command after a Roman number for details about the number"
  echo
  echo -e "\e[1;32m How to use this program?\e[;m"
  echo "    to use this program simply run it and add a number after the name of the program"
  echo "    it can be a regular number or a Roman number"
  echo "    the program will detect it and return the opposite number"
  echo "    Have fun :)"
  echo

#the --about option
elif [[ $1 == "-a" || $1 == "--about" ]]; then
  echo
  echo -e "\e[1;36mAbout\e[;m"
  echo
  echo "  this script was created for me learn bash"
  echo "  to see more of my work you can visit: https://gitlab.com/TzintzeneT"
  echo
  echo "  to learn more about this program use the [--help] option"
  echo

#the --values option
elif [[ $1 == "-v" || $1 == "--val" || $1 == "--values" ]]; then
  echo
  echo -e "\e[1;36mValues\e[;m"
  echo
  echo -e "\e[1;32m  I:\e[;m 1"
  echo -e "\e[1;32m  V:\e[;m 5"
  echo -e "\e[1;32m  X:\e[;m 10"
  echo -e "\e[1;32m  L:\e[;m 50"
  echo -e "\e[1;32m  C:\e[;m 100"
  echo -e "\e[1;32m  D:\e[;m 500"
  echo -e "\e[1;32m  M:\e[;m 1000"
  echo
  echo -e "\e[1;31m  *\e[;mletters/digits will become negative if they appear before a bigger letter/digit than them (LTR)"
  echo

#choose a function to calculate the number
elif [[ $1 =~ ^[-+]?[0-9]+$ ]]; then
  #echo "Input is an number"
  toRoman
else
  #echo "Input is Roman"
  fromRoman
fi
