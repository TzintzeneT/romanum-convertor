# RomaNum Convertor

## About this project
This is a simple utility for converting Roman numbers to dec numbers and vice versa, you enter one and this program will return the other. <br />
this project was a simple project for me to learn how to use bash, I hope you will enjoy it.

## How to install?
1. download (and extract) the source code from the [latest release](https://gitlab.com/TzintzeneT/romanum-convertor/-/releases)
2. Open the directory you downloaded the source code to in the terminal
3. run the install.sh script or use the commands below to install the program

- copy the file:
`cp RomanNumbers.sh romanum`
- give the file execute permission:
`chmod +x romanum`
- and finaly, move the file to the /bin directory so that all users can use it:
`sudo mv romanum /bin/`
- you can now use the utility using the 'romanum' command

## How to uninstall?
simply run the command:
`sudo rm /bin/romanum`

## Created using FOSS
Because foss is awesome

## License
this project use the GNU GPL V3 license
